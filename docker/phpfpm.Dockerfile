FROM php:7.2.19-fpm-alpine

RUN apk update && \
    apk add git zip

RUN curl --silent --show-error https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

RUN apk add mariadb-client && \
    docker-php-ext-install pdo && \
    docker-php-ext-install pdo_mysql
