# Laravel 6.2 Php:7.2.19 Nginx 1.15 Docker Demo
* your laravel application should be at /app
* this was tested with laravel 6.2 but can be also used for other versions (feel free to try it out)
* ![laravel_demo](laravel_running_on_demo.png)


## Requirements
this repo was tested on the ff:
* Docker v19.03.8 (atleast)
* docker-compose 1.17.1

## to run
```bash
docker-compose build
docker-compose up -d
docker-compose exec phpfpm mkdir -p /var/www/html/storage/logs
docker-compose exec phpfpm mkdir -p /var/www/html/storage/framework/cache
docker-compose exec phpfpm mkdir -p /var/www/html/storage/framework/sessions
docker-compose exec phpfpm mkdir -p /var/www/html/storage/framework/views
docker-compose exec phpfpm composer install --no-scripts
docker-compose exec phpfpm chown www-data:www-data -Rf /var/www/html/storage
docker-compose exec phpfpm chown www-data:www-data -Rf /var/www/html/bootstrap
```
then you can see your application at http://0.0.0.0:8282
```
curl -I http://0.0.0.0:8282
```

## to help you manager your docker stuff (images/containers/etc)
* currently for linux only
* ```bash
  # run portainer
  # web UI will be accessible from http://0.0.0.0:9000/
  bash run-portainer.sh
  ```
* visit portainer admin dashboard http://0.0.0.0:9000/
